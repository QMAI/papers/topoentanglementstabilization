# TopoEntanglementStabilization

Code for the manuscript "Topological Entanglement Stabilization in Superconducting Quantum Circuits".

## Getting started

This notebook is organized as the follwoing:

First we declare the functions and parameters

Next we initialize the numerics:

    - 1. random hamiltonian generation
    - 2. qubit projection, negativity calculation
    - 3. initialize 100 random in topological and trivial regime respectively and do the statistics
    
The code needs to be run with Julia, preferably `Julia 1.7`.